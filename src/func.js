const getSum = (str1, str2) => {
  if(typeof str1 !== "string" || typeof str2 !== "string"){
    return false;
  }

  if((!(/^\d+$/.test(str1)) && str1.length > 0) || (!(/^\d+$/.test(str2)) && str2.length > 0)){
    return false;
  }
  var num1 = 0;
  var num2 = 0;

  if(str1.length === 0){
    num1 = 0;
  }
  else{
    num1 = parseInt(str1);
  }

  if(str2.length === 0){
    num2 = 0;
  }
  else{
    num2 = parseInt(str2);
  }

  return(String(num1 + num2));
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var posts = 0;
  var comments = 0;
  for(var i = 0; i < listOfPosts.length; i++){
    if(listOfPosts[i]["author"] == authorName){
      posts += 1;
    }

    if("comments" in listOfPosts[i]){
      for(var j = 0; j < listOfPosts[i]["comments"].length; j++){
        if(listOfPosts[i]["comments"][j]["author"] == authorName){
          comments += 1;
        }
      }
    }
  }

  return "Post:" + posts + ",comments:" + comments;
};

const tickets=(people)=> {
  var dollarBills25 = 0;
  var dollarBills50 = 0;

  for(var i = 0; i < people.length; i++){
    if(parseInt(people[i]) == 25){
      dollarBills25 += 1;
    }
    
    else if(parseInt(people[i]) == 50){
      if(dollarBills25 >= 1){
        dollarBills50 += 1;
        dollarBills25 -= 1;
      }
      else{
        return "NO";
      }
    }

    else{
      if(dollarBills50 >= 1 && dollarBills25 >= 1){
        dollarBills25 -=1;
        dollarBills50 -=1;
      }

      else if(dollarBills25 >= 3){
        dollarBills25 -= 3;
      }

      else{
        return "NO";
      }
    }
  
  }

  return "YES";

};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
